% Default settings, and unit color range:
PsychDefaultSetup(2);

nonsymmetric = 0;
benchmark = 0;

% Initial stimulus params for the gabor patch:
res = 3*[323 323];
phase = 0;
sc = 100.0;
freq = .01;
tilt = 0;
contrast = 1;
aspectratio = 1.0;

% Disable synctests for this quick demo:
oldSyncLevel = Screen('Preference', 'SkipSyncTests', 2);

% Choose screen with maximum id - the secondary display:
screenid = max(Screen('Screens'));

PsychImaging('PrepareConfiguration');

% Setup imagingMode and window position/size depending on mode:
PsychImaging('AddTask','General', 'FloatingPoint32Bit');
rect = [];

% Open a fullscreen onscreen window on that display, choose a background
% color of 0.5 = gray with 50% max intensity:
win = PsychImaging('OpenWindow', screenid, 0.5, rect);

tw = res(1);
th = res(2);
x=tw/2;
y=th/2;

ycenter = 1080/2;
xcenter = 1920/2;

% Build a procedural gabor texture for a gabor with a support of tw x th
% pixels, and a RGB color offset of 0.5 -- a 50% gray.
[gabortex, gaborrect] = CreateProceduralGabor(win, tw, th, nonsymmetric, [0.5 0.5 0.5 0.0], 1, 0.5);

% noies image for now. exact set of pixels will tend to get overwritten on
% each trial
noiseimg = randn(floor(tw*2));
filted = imgaussfilt(noiseimg, 20, 'Padding','circular');
filted = normalize(filted, 'range');
noisetex = Screen('MakeTexture', win, filted,[],[],[]);

aperture_rect = SetRect(0, 0, floor(tw), floor(tw));
aperture = Screen('OpenOffscreenWindow', win, 0.5);

% [masktex, maskDstRects] = CreateProceduralSmoothedDisc(win, tw*4, tw*4, [], tw, tw, true, 0);
Screen('FillOval', aperture, [1 1 1 0], CenterRectOnPoint(ScaleRect(aperture_rect,.6,.6),xcenter/2,ycenter));
% Screen('DrawTextures', aperture, masktex, [], CenterRectOnPoint(maskDstRects,xcenter/2,ycenter), [], [], 1, [0, 0, 0, 1]', [], []);

% Perform initial flip to gray background and sync us to the retrace:
vbl = Screen('Flip', win);
ts = vbl;
count = 0;
totmax = 0;

% Animation loop: Run for 10000 iterations:
count = count + 1;

% Set new rotation angle:
tilt = count/10;

% Drift phase and aspectratio as well...
phase = count * 10;
aspectratio = 1 + count * 0.01;

% Draw the Gabor patch: We simply draw the procedural texture as any other
% texture via 'DrawTexture', but provide the parameters for the gabor as
% optional 'auxParameters'.
Screen('DrawTexture', win, gabortex, [], CenterRectOnPoint(gaborrect, 1920/4, 1080/2), 90+tilt, [], [], [], [], ...
    kPsychDontDoRotation, [180-phase, freq, sc, contrast, aspectratio, 0, 0, 0]);

Screen('DrawDots', win, [xcenter, ycenter], 3.261304191*10, 1, [], 2);

Screen('Flip', win);

img = Screen('GetImage', win, []);
imwrite(img, 'gabor.png');

% draw gabor at center, no fixation
Screen('DrawTexture', win, gabortex, [], [], 90+tilt, [], [], [], [], ...
    kPsychDontDoRotation, [180-phase, freq, sc, contrast, aspectratio, 0, 0, 0]);
Screen('Flip', win);

img = Screen('GetImage', win, CenterRectOnPoint([0,0,400,400], ycenter, xcenter));
imwrite(img, 'gabor-center.png');


% Turn on blendfunction for antialiasing of drawing dots
Screen('BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');


% Screen('DrawTexture', win, noisetex, [], CenterRectOnPoint(noiserect, 1920/4, 1080/2), [], [], [], [], [], [], [1, 123, 0, 0]);
% Screen('DrawTexture', win, aperture, [], CenterRectOnPoint(aperture_rect, 1920/4, 1080/2));

Screen('DrawTexture', win, noisetex, []);
Screen('DrawTexture', win, aperture, [], []);


Screen('DrawDots', win, [xcenter, ycenter], 3.261304191*10, 1, [], 2);

Screen('Flip', win);
img = Screen('GetImage', win, []);
imwrite(img, 'mask.png');

Screen('FrameArc', win, 0, [1920/2 - 1080/4, 1080/4, 1920/2 + 1080/4, 3*1080/4], 0, 360, 5, 5)

Screen('Flip', win);
img = Screen('GetImage', win, []);
imwrite(img, 'circle.png');


% Close window, release all ressources:
sca;
% Done.


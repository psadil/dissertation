% Setup defaults and unit color range:
PsychDefaultSetup(2);

% Disable synctests for this quick demo:
oldSyncLevel = Screen('Preference', 'SkipSyncTests', 2);

% Choose screen with maximum id - the secondary display:
screenid = max(Screen('Screens'));

% Open a window 
PsychImaging('PrepareConfiguration');
[win, winrect] = PsychImaging('OpenWindow', screenid, 0.5);

xcenter = RectWidth(winrect)/2;
ycenter = RectHeight(winrect)/2;

% Initial stimulus params for the grating:
xres = 300;
yres = 200;
phase = 0;
freq = .08;
tilt = 10;
contrast = 1;
sep = 20;
padding = 10;


% Build a procedural gabor texture for a grating with a support of tw x th
% pixels, and a RGB color offset of 0.5 -- a 50% gray.
[tex, rect] = CreateProceduralSineGrating(win, xres, yres, [.5 .5 .5 0], [], 0.5);

rect_centered = CenterRectOnPoint(rect, xcenter, ycenter);

%% draw inducer

Screen('DrawTexture', win, tex, [], OffsetRect(rect_centered, -(xres/2 + sep/2), 0), ...
    -tilt, [], [], [], [], kPsychUseTextureMatrixForRotation, [phase, freq, contrast, 0]);
Screen('DrawTexture', win, tex, [], OffsetRect(rect_centered, (xres/2 + sep/2), 0), ...
    tilt, [], [], [], [], kPsychUseTextureMatrixForRotation, [phase, freq, contrast, 0]);

% Turn on blendfunction for antialiasing of drawing dots
Screen('BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

Screen('DrawDots', win, [xcenter, ycenter], 10, 1, [], 2);
Screen('BlendFunction', win, 'GL_ONE', 'GL_ZERO');

% Perform initial flip to gray background and sync us to the retrace:
Screen('Flip', win);

img = Screen('GetImage', win, CenterRectOnPoint([0,0,xres*2+sep+padding, yres+padding],xcenter,ycenter));
imwrite(img, 'tae-inducer.png');


%% draw test
Screen('DrawTexture', win, tex, [], OffsetRect(rect_centered, -(xres/2 + sep/2), 0), ...
    0, [], [], [], [], [], [phase, freq, contrast, 0]);
Screen('DrawTexture', win, tex, [], OffsetRect(rect_centered, (xres/2 + sep/2), 0), ...
    0, [], [], [], [], [], [phase, freq, contrast, 0]);

% Turn on blendfunction for antialiasing of drawing dots
Screen('BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
Screen('DrawDots', win, [xcenter, ycenter], 10, 1, [], 2);
Screen('BlendFunction', win, 'GL_ONE', 'GL_ZERO');

% Perform initial flip to gray background and sync us to the retrace:
Screen('Flip', win);

img = Screen('GetImage', win, CenterRectOnPoint([0,0,xres*2+sep+padding, yres+padding],xcenter,ycenter));
imwrite(img, 'tae-test.png');

% Close window, release all ressources:
sca

% Restore old settings for sync-tests:
Screen('Preference', 'SkipSyncTests', oldSyncLevel);


# Building Figures

The analyses come from multiple projects, meaning that they are spread out. Analysis scripts are stored in submodules. I have ensured that the git commit of the submodules contain the iles that produced the final versions of the figures. However, this does not necessarily mean that the figures could be immediately reproduced from the cloned submodules. Many of the figures are the results of a long-running analysis, cached with [drake](https://docs.ropensci.org/drake/). The caches are stored only on the lab server (and its backups), not in the git repositories.

Figures were built in two ways: either directly with tikz or as R figures which were then exported using `tikzDevice`. The latter includes both figures that were drawn entirely from scratch (e.g., 2.1, showing a few example models), or as exported analyses. 

Below, I list the files that were used to generate each of the figures. Figures listed `.tikz` should be easy to trace, given that their source will be in figures/ and that source will be imported into the document. Files with a `.R` extension export something with `tikzDevice`. 

## Chapter 1

1.1a tikz
1.1bc tools/fig-1bc.R
1.2 tikz
1.3 tools/dvm-sums.R
1.4 tikz
1.5 submodules/serialdependenceanalysis/tools/exp-maskedclick-splits_plot.R
1.6 submodules/serialdependenceanalysis/tools/exp-maskedclick-post_plot.R
1.7 submodules/serialdependenceanalysis/tools/exp-most-posts_plots.R
1.8 submodules/serialdependenceanalysis/tools/plot-post-dissertate.R

## Chapter 2

1.1 tools/labeled-lines2.R
1.2 

# Building Document

To build the pdf, I relied heavily on configurations in [TexStudio](http://texstudio.sourceforge.net/). Some of the figures need to be compiled with lualatex. However, using that engine produced pdfs in which the title fonts were weird (e.g., on the title page, the letter sizing was always off when the pdf was viewed on Windows). Pdflatex produced better documents. pdflatex can be used by first compiling with lualatex, which will generate a cache of the figures, and then removing the lines about the tikz graphdrawing library.   

\documentclass[./main.tex]{subfiles}

\begin{document}

\label{app:bayes}

To account for the circular nature of orientation, the commonly used derivative of Gaussian model was replaced with a derivative of the density function of a von Mises distribution. The two derivatives look similar and have the same number of parameters (with the same interpretation) but working with the von Mises ensures that function is continuous at \ang{\pm90}.\footnote{The density function for a von Mises is periodic every $2\pi$, but gratings are period every $\pi$. To account for this, orientations were always doubled during model fitting.}

The density function of a von Mises distribution with a width parameter, $w$, for an orientation, $x$ (ignoring for a moment, whether the orientation is reported or presented), centered on orientation 0 is given by 

\begin{equation} % \label{eq:dvm}
f(x | w) = \frac{\exp\left( w \cos\left( x \right) \right)}{I_0(w)}
\end{equation}

In that density function, $I_0(\cdot)$, is the modified Bessel function of the first kind of order 0. It normalizes the density function so that its integral is 1 between $[-\pi,\pi)$. The derivative of this density function with respect to $x$ is equal to the following.

\begin{equation} % \label{eq:dvm}
f'(x | w) = \frac{w \sin \left(x\right) \exp\left( w \cos\left( x \right) \right)}{I_0(w)}
\end{equation}

This derivative has only a single parameter, which governs both the width and height. To serve as a model for serial dependence, this derivative must be rescaled. The resulting function (Equation \ref{eq:dvm}) gives the component of the error on each trial that is due to a dependence. The cause of this dependence, $x$, may be the difference between the target orientation and either the inducing orientation or the response to that orientation, depending on which dependencies are included in a model.

\begin{equation} \label{eq:dvm}
f(x|a,w) = \frac{a w \sin \left(x\right) \exp\left( w \cos\left( x \right) \right)}{I_0(w) f' \left( 2\arctan \left( \sqrt{\sqrt{4w^2+1}-2w} \right)  \middle| w \right)}
\end{equation}

The value $2 \arctan \left( \sqrt{\sqrt{4w^2+1}-2w} \right)$ is the orientation at which the derivative peaks, itself a function of $w$. So, the rescaling divides the derivative by its maximum (forcing the maximum to 1) and then multiplies by $a$. With this rescaling, the parameter $a$ can be interpreted as the maximum error, which is equivalent to the amplitude of the derivative of Gaussian\footnote{Note that this rescaled derivative can be simplified further -- optimized for computation during model fitting. It is presented in this relatively raw form since the simplification does not lend further extra insight into the function.}. 

\begin{figure}
	\centering
	%  \inputtikz{exp-all-oblique}
	\includegraphics{images/oblique.png}
	\caption{Errors were largest on orientations intermediate to the cardinal and oblique axes. Each point corresponds to the error on a single trial. The x-axis follows the convention that \ang{0} and \ang{180} are horizontal and increasingly positive orientations are more counterclockwise. Blue lines indicate best fitting sinusoids (minimum squared error).}
	\label{fig:oblique}
\end{figure}

Two additional sources of bias were incorporated into all models (i.e,. all versions of the von Mises models and the spline model). First, participants may exhibit a general clockwise or counterclockwise bias, an offset that affects all orientations equally (all prior studies analyzing serial dependence that account for this bias do so in a preprocessing step, removing the average orientation from each participant prior to model fitting). Second, when reporting orientations, humans tend to be more erroneous on those orientations which are intermediate to the cardinal and oblique axes \parencite{appelle_perception_1972, jastrow_studies_1892, wei_bayesian_2015}. This anisotropy was also present in Experiment 1 (Figure \ref{fig:oblique}). Some serial dependence studies have accounted for this anisotropy through preprocessing \parencite{pascucci_laws_2019}, but not all. I accounted for it in the Bayesian model by including a sinusoidal term that cycled twice (e.g., contained two peaks between 0 and \ang{180}). A schematic of the hierarchical Bayesian model is given in Figure \ref{fig:sdmodel}. This diagram is designed to give a high-level overview of the relationships among parameters, and how the model relates these parameters to the data. The distributions assigned to each parameter that is not a prior is listed to the right of the diagram, and the priors are listed with the square notes. 

\begin{figure}
  \centering
  \inputtikz{sd-model}
%   \input{figures/sd-model}
%   \tikzsetnextfilename{sd-model}
%   \includegraphics[width=\linewidth]{figures/sd-model.tikz}
  \caption{Hierarchical Bayesian Model of Serial Dependence. Observed data, $y$, are indicated with a shaded node. They were modeled with a normal distribution with standard deviation $\sigma$ and location $\mu$. The parameter $\mu$ is the output of a deterministic function, the summation of biases due to the oblique effect, clockwise/counterclockwise biases, and serial dependencies caused by either the previous response, the previous orientation, or both. Nodes are grouped with the square ``plates'', indicating over which subsets of the data the node is replicated. The dashed plate around $a$ and $w$ is to indicate where the three von Mises models differed; in two models, there was a single $x$, but in the full model there were two kinds $x$. The magnitudes of the summands are given by $\beta$, $\gamma$, and $a$. The parameter $a$ is the amplitude of the rescaled derivative of von Mises with width $w$ (Equation \ref{eq:dvm}). These four parameters -- $\beta$, $\gamma$, $a$, and $w$ -- were estimated for each participant, hierarchically. These hierarchies were modeled with a normal distribution for each of $\beta$, $\gamma$, and $a$, and a half-normal distribution for $w$. The location and scale of these normal (or truncated-normal, truncated at 0) distributions are given by a $\mu$ and $\sigma$ in the diagram, respectively. The priors on the population-level effects are given by the filled square nodes. $N(\mu,\sigma)$: Normal with location $\mu$ and scale $\sigma$; $TN$: truncated-normal location $\mu$ and scale $\sigma$; $\Gamma(\zeta,\tau)$: Gamma with shape $\zeta$ and rate $\tau$.}
  \label{fig:sdmodel}
\end{figure}

\end{document}